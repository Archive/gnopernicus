/* src-kbd.h
 *
 * Copyright 2001, 2002 Sun Microsystems, Inc.,
 * Copyright 2001, 2002 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __SRC_KBD_H__
#define __SRC_KBD_H__

#include <glib.h>
#include "SREvent.h"
#include "libke.h"

#define KE_LAYER_TIME_OUT 1
#define KE_LAYER_CHANGED 0


#define KeyboardEchoCB SROnEventProc

typedef struct _SRHotkeyData
{
    KEKeyModifier modifiers; 
    gchar *keystring; 
} SRHotkeyData;

gboolean 	src_kbd_init (KeyboardEchoCB kecb);
void 		src_kbd_terminate ();
gboolean	src_kbd_restart_cmds ();

#endif /* __SRC_KBD_H__ */
