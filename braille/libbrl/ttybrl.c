/* ttybrl.c
 *
 * Copyright 2003 - 2005 Mario Lang
 * Copyright 2003 - 2005 BAUM Retec A.G.
 * Copyright 2003 - 2005 Sun Microsystems Inc. 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* This file implements an interface to BRLTTY's BrlAPI
 *
 * BrlAPI implements generic display access.  This means
 * that every display supported by BRLTTY should work via this
 * driver.
 */


#include <ttybrl.h>
#include <ttybrl-wrap.h>
#include <glib.h>
#include <gmodule.h>

#ifdef BRLTTY_SUPPORT

static GModule *brltty_brl_module = NULL;

void
brltty_init ()
{
    brltty_brl_module = g_module_open ("brltty-wrap", G_MODULE_BIND_LAZY | G_MODULE_BIND_LOCAL);
    if (!brltty_brl_module)
    {
	gchar *path = g_module_build_path (BRLTTY_WRAP_LIB_PATH, "brltty-wrap");
	brltty_brl_module = g_module_open (path, G_MODULE_BIND_LAZY | G_MODULE_BIND_LOCAL);
	g_free (path);
    }
}

void
brltty_terminate ()
{
    if (brltty_brl_module)
	g_module_close (brltty_brl_module);
    brltty_brl_module = NULL;
}

gboolean
brltty_check_if_present ()
{
    return brltty_brl_module != NULL;
}

typedef gint (*BrlTTYOpen) (gchar          *device_name, 
			    const gchar     *port,
			    BRLDevCallback  device_callback,
			    BRLDevice       *device);

gint
brltty_brl_open_device (gchar          *device_name, 
			const gchar     *port,
			BRLDevCallback  device_callback,
			BRLDevice       *device)
{
    gboolean rv;
    BrlTTYOpen brltty_open;

    g_assert (brltty_check_if_present ());

    rv = g_module_symbol (brltty_brl_module, 
				    "brltty_wrap_brl_open_device",
				    (gpointer *)&brltty_open);
    g_assert (rv && brltty_open);
    return brltty_open (device_name, port, device_callback, device);
}

#else /* ! BRLTTY_SUPPORT */

void
brltty_init ()
{
}

void
brltty_terminate ()
{
}

gboolean
brltty_check_if_present ()
{
    return FALSE;
}

gint
brltty_brl_open_device (gchar          *device_name, 
			    const gchar     *port,
			    BRLDevCallback  device_callback,
			    BRLDevice       *device)
{
    return 0;
}

#endif /* BRLTTY_SUPPORT */
