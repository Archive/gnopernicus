/* libke.h
 *
 * Copyright 2001 - 2005 Sun Microsystems, Inc.,
 * Copyright 2001 - 2005 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __LIBKE_H__
#define __LIBKE_H__

#include <glib.h>

typedef enum
{
    KE_MODIFIER_UNMODIFIED	= 0,
    KE_MODIFIER_ALT 		= (1 << 0),
    KE_MODIFIER_CONTROL		= (1 << 1),
    KE_MODIFIER_SHIFT		= (1 << 2)
}KEKeyModifier;

typedef enum
{
    KE_KEY_PRESSED,
    KE_KEY_RELEASED
}KEKeyAction;

typedef void (*KeReportKeyboardGrab) 	(const gchar *key, KEKeyModifier modifier);
typedef void (*KeReportCommand) 	(const gchar *key, KEKeyModifier modifier);
typedef void (*KeReportKeyboardHit) 	();
typedef void (*KeReportCursor) 		(const gchar *cursor);
typedef void (*KeReportModifier) 	(const gchar *modifier, KEKeyAction action);
typedef void (*KeReportLayerChanged)	(gint layer);
typedef void (*KeReportLayerBack) 	(gint layer);
typedef void (*KeReportLayerKey) 	(gint layer, gint key);
typedef void (*KeReportMouseMove) 	(gint x, gint y);

typedef struct
{
    KeReportKeyboardGrab grab;
    KeReportKeyboardHit	 hit;
    KeReportCommand	 command;
    KeReportCursor 	 cursor;
    KeReportModifier 	 modifier;
    KeReportLayerKey 	 layer_key;
    KeReportLayerChanged layer_ch;
    KeReportLayerBack 	 layer_back;
    KeReportMouseMove	 mouse_move;
}KECallbacks;

typedef enum _KEMouseButton
{
    KE_MOUSE_BUTTON_LEFT,	
    KE_MOUSE_BUTTON_RIGHT,
    KE_MOUSE_BUTTON_MIDDLE
}KEMouseButton;

typedef enum _KEMouseButtonAction
{
    KE_MOUSE_BUTTON_PRESS,	
    KE_MOUSE_BUTTON_RELEASE,
    KE_MOUSE_BUTTON_CLICK,
    KE_MOUSE_BUTTON_DOUBLECLICK
}KEMouseButtonAction;

gboolean     ke_init 	  	    (const KECallbacks *callbacks);
void 	     ke_terminate 	    ();

gboolean     ke_command_add 	    (const gchar *key, KEKeyModifier modifier);
void         ke_commands_remove_all ();

gboolean     ke_start_keyboard_grab ();
void         ke_stop_keyboard_grab  ();

gboolean     ke_mouse_move 	    (gint x, gint y);
gboolean     ke_mouse_button_action (KEMouseButton button, KEMouseButtonAction action);

#endif /* __LIBKE_H__ */
