/* sru-debug.h
 *
 * Copyright 2001 - 2005 Sun Microsystems, Inc.,
 * Copyright 2001 - 2005 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SRU_MODULE_H__
#define __SRU_MODULE_H__

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <glib.h>

#define SR_DEBUG /* FIXME */

#if defined SR_DEBUG && ! defined SRU_MODULE
#  define SRU_MODULE
#endif /* defined SR_DEBUG && ! defined SRU_MODULE */

G_BEGIN_DECLS

typedef enum _SRUModuleFlag
{
    SRU_MODULE_FLAG_UNINITIALIZED,
    SRU_MODULE_FLAG_INITIALIZED,
}SRUModuleFlag;


#ifdef SRU_MODULE

#define SRU_MODULE_FLAG_DEFS(flag)	static SRUModuleFlag flag = SRU_MODULE_FLAG_UNINITIALIZED;

#define sru_module_flag_set_initialized(flag)     (flag) = SRU_MODULE_FLAG_INITIALIZED
#define sru_module_flag_set_uninitialized(flag)   (flag) = SRU_MODULE_FLAG_UNINITIALIZED

#define sru_module_flag_check_initialized(flag)   ((flag) == SRU_MODULE_FLAG_INITIALIZED)
#define sru_module_flag_check_uninitialized(flag) ((flag) == SRU_MODULE_FLAG_UNINITIALIZED)


#else /* SRU_MODULE */

#define SRU_MODULE_FLAG_DEFS(flag)

#define sru_module_flag_set_initialized(flag)
#define sru_module_flag_set_uninitialized(flag)

#define sru_module_flag_check_initialized(flag)
#define sru_module_flag_check_uninitialized(flag)


#endif /* SRU_MODULE */

G_END_DECLS

#endif /* __SRU_MODULE_H__ */
