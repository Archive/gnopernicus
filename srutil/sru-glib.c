/* sru-glib.c
 *
 * Copyright 2001 - 2005 Sun Microsystems, Inc.,
 * Copyright 2001 - 2005 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <glib.h>

#include "sru-glib.h"

void
sru_slist_foreach (GSList *list, 
		   SRUProcessFunction process)
{
    GSList *crt;

    g_assert (list && process);

    for (crt = list; crt; crt = crt->next)
	process (crt->data);
}

void
sru_slist_terminate (GSList *list,
		     SRUProcessFunction terminate)
{
    sru_slist_foreach (list, terminate);
    g_slist_free (list);
}
