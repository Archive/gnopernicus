/* sru-glib.h
 *
 * Copyright 2001 - 2005 Sun Microsystems, Inc.,
 * Copyright 2001 - 2005 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SRU_GLIB_H__
#define __SRU_GLIB_H__

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <glib.h>

G_BEGIN_DECLS

typedef void (*SRUProcessFunction) (gpointer data);

void sru_slist_foreach (GSList *list, SRUProcessFunction process);
void sru_slist_terminate (GSList *list, SRUProcessFunction terminate);

G_END_DECLS

#endif /* __SRU_GLIB_H__ */
