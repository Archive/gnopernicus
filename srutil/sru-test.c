/* sru-test.c
 *
 * Copyright 2001 - 2005 Sun Microsystems, Inc.,
 * Copyright 2001 - 2005 BAUM Retec, A.G.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdio.h>
#include <string.h>
#include <glib.h>

#include "sru-test.h"

static gint
srut_get_selection_index (gchar c,
			  SRUTMenuEntry *entries,
			  gint len)
{
    gint i;

    g_assert (entries && len > 0);

    for (i = 0; i < len; i++)
    	if (entries[i].selection == c)
	    return i;

    if (c == '0')
	return -1;

    return -2;
}

void
srut_run_menu (SRUTMenuEntry *entries,
	       gint length)
{
    gint i, index;	

    g_assert (entries && length > 0);

    do
    {
	g_print ("\n\n\n");
	for (i = 0; i < length; i++)
    	    g_print ("\n\t%c. %s", entries[i].selection, entries[i].message);
	g_print ("\n\t0. Exit");
	g_print ("\n");

	do
	{
	    gchar opt[1001];
	    gint len;
	    g_print ("Choose an option: ");
	    gets (opt);
	    len = strlen (opt);
	    index = srut_get_selection_index (opt[len-1], entries, length);
	}while (index == -2);

	if (index >= 0)
	    entries[index].action ();
    }while (index != -1);
}
